package ictgradschool.web.lab12.ex1;

import ictgradschool.web.lab12.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try(FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while(true) {
            System.out.println("Please input a search pattern:");
            System.out.print("> ");
            String pattern = Keyboard.readInput();

            // Set the database name to your database
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                System.out.println("Connection successful");

                try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM simpledao_articles WHERE title LIKE ?;")) {
                    pattern = "%" + pattern + "%";
                    stmt.setString(1, pattern);

                    try (ResultSet r = stmt.executeQuery()) {
                        while (r.next()) {
                            System.out.println(r.getString("body"));
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println("---------------------------------------");
        }
    }
}
