package ictgradschool.web.lab12.ex3;


import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import ictgradschool.web.lab12.Keyboard;
import org.jooq.*;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;

import javax.xml.bind.JAXB;

import static ictgradschool.web.lab12.ex3.generated.Tables.*;

public class Exercise03 {
    public static void main(String[] args) {

        String mode = "";
        String key;
        boolean nullMode = false;
        while (true) {
            System.out.println("Welcome to the Film database!\n" +
                    "Please select an option from the following:\n" +
                    "1. Information by Actor\n" +
                    "2. Information by Movie\n" +
                    "3. Information by Genre\n" +
                    "4. Exit");
            System.out.print("> ");
            mode = Keyboard.readInput();
            if(mode == null){
                System.out.println("The input is null!");
                if(nullMode){
                    System.out.println("There is still a problem");
                    System.out.println("Exit...");
                    break;
                } else {
                    System.out.println("Probably there is some problem with your input device.");
                    System.out.println("Please try again...");
                    nullMode = true;
                }
            }
            else if (mode.equals("1")) {
                String input;
                while(true){
                    System.out.println("Please enter the name of the actor you wish to get information about, or press enter to return to the previous menu");
                    System.out.print("> ");
                    input = Keyboard.readInput();
                    if(input.isEmpty()){
                        break;
                    }
                    runSearch("Actor", input);
                }
            } else if (mode.equals("2")) {
                String input;
                while(true){
                    System.out.println("get information about, or press enter to return to the previous menu");
                    System.out.print("> ");
                    input = Keyboard.readInput();
                    if(input.isEmpty()){
                        break;
                    }
                    runSearch("Movie", input);
                }
            } else if (mode.equals("3")) {
                String input;
                while(true){
                    System.out.println("Please enter the name of the genre you wish to get information about, or press enter to return to the previous menu");
                    System.out.print("> ");
                    input = Keyboard.readInput();
                    if(input.isEmpty()){
                        break;
                    }
                    runSearch("Genre", input);
                }
            } else if (mode.equals("4")) {
                System.out.println("Thank you for use.");
                break;

            } else {
                System.out.println("Input error!");
                System.out.println("Please input 1 - 4.");
                System.out.println();
            }

        }

    }

    private static void runSearch(String key, String value) {
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch (key.toLowerCase()){
            case "actor":
                searchActor(dbProps, value);
                break;
            case "movie":
                searchMovie(dbProps, value);
                break;
            case "genre":
                searchGenre(dbProps, value);
                break;
        }

    }

    private static void searchActor(Properties dbProps, String value){
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            Result<Record3<Integer, String, String>> result = create.select(PFILMS_ACTOR.ACTOR_ID, PFILMS_ACTOR.ACTOR_FNAME, PFILMS_ACTOR.ACTOR_LNAME)
                    .from(PFILMS_ACTOR)
                    .where(PFILMS_ACTOR.ACTOR_FNAME.like("%" + value + "%"))
                    .or(PFILMS_ACTOR.ACTOR_LNAME.like("%" + value + "%")).fetch();

            if(result.isEmpty()){
                System.out.println("Sorry, we couldn't find any actor by that name.");
                System.out.println();
            } else {
                for (Record3<Integer, String, String> r : result) {
                    String name = r.getValue(PFILMS_ACTOR.ACTOR_FNAME) + r.getValue(PFILMS_ACTOR.ACTOR_LNAME);
                    System.out.println(name + " is listed as being involved in the following films:");
                    Result<Record2<String, String>> resultFilm = create.select(PFILMS_FILM.FILM_TITLE, PFILMS_ROLE.ROLE_NAME)
                            .from(PFILMS_FILM)
                            .join(PFILMS_PARTICIPATES_IN)
                            .on(PFILMS_FILM.FILM_ID.equal(PFILMS_PARTICIPATES_IN.FILM_ID))
                            .join(PFILMS_ROLE)
                            .on(PFILMS_PARTICIPATES_IN.ROLE_ID.equal(PFILMS_ROLE.ROLE_ID))
                            .where(PFILMS_PARTICIPATES_IN.ACTOR_ID.equal(r.getValue(PFILMS_ACTOR.ACTOR_ID)))
                            .fetch();
                    System.out.println();
                    for (Record2<String, String> rf : resultFilm) {
                        System.out.println(String.format("%s(%s)",rf.getValue(PFILMS_FILM.FILM_TITLE), rf.getValue(PFILMS_ROLE.ROLE_NAME)));
                    }
                    System.out.println();

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void searchMovie(Properties dbProps, String value){
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);
            Result<Record3<Integer, String, String>> result = create.select(PFILMS_FILM.FILM_ID, PFILMS_FILM.FILM_TITLE, PFILMS_FILM.GENRE_NAME)
                    .from(PFILMS_FILM)
                    .where(PFILMS_FILM.FILM_TITLE.like("%"+value+"%"))
                    .fetch();

            if(result.isEmpty()){
                System.out.println("Sorry, we couldn't find any movie by that name.");
            } else {
                for (Record3<Integer, String, String> r : result) {
                    System.out.println(String.format("The film %s is a %s movie that features the following people:", r.getValue(PFILMS_FILM.FILM_TITLE), r.getValue(PFILMS_FILM.GENRE_NAME)));
                    Result<Record3<String, String, String>> resultActor = create
                            .select(PFILMS_ACTOR.ACTOR_FNAME, PFILMS_ACTOR.ACTOR_LNAME,PFILMS_ROLE.ROLE_NAME)
                            .from(PFILMS_ACTOR)
                            .join(PFILMS_PARTICIPATES_IN)
                            .on(PFILMS_PARTICIPATES_IN.ACTOR_ID.equal(PFILMS_ACTOR.ACTOR_ID))
                            .join(PFILMS_ROLE)
                            .on(PFILMS_ROLE.ROLE_ID.equal(PFILMS_PARTICIPATES_IN.ROLE_ID))
                            .where(PFILMS_PARTICIPATES_IN.FILM_ID.equal(r.getValue(PFILMS_FILM.FILM_ID)))
                            .fetch();

                    for (Record3<String, String, String> ra : resultActor) {
                        System.out.println(ra.getValue(PFILMS_ACTOR.ACTOR_FNAME)+" "
                                            +ra.getValue(PFILMS_ACTOR.ACTOR_LNAME)+"("
                                            +ra.getValue(PFILMS_ROLE.ROLE_NAME)+")");
                    }
                    System.out.println();
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void searchGenre(Properties dbProps, String value){
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);
            Result<Record1<String>> result = create.select(PFILMS_FILM.FILM_TITLE)
                    .from(PFILMS_FILM)
                    .where(PFILMS_FILM.GENRE_NAME.like(value))
                    .fetch();

            if(result.isEmpty()){
                System.out.println("Sorry, we couldn't find any movie by that genre.");
            } else {
                System.out.println(String.format("The %s genre includes the following films:",
                        value.substring(0, 1).toUpperCase() + value.substring(1).toLowerCase()));
                for (Record1<String> r : result) {
                    System.out.println(r.getValue(PFILMS_FILM.FILM_TITLE));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
