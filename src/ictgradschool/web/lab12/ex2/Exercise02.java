package ictgradschool.web.lab12.ex2;

import ictgradschool.web.lab12.Keyboard;

import java.util.List;

public class Exercise02 {
    public static void main(String[] args) {
        SimpledaoArticlesDAO articlesDAO = new SimpledaoArticlesDAO();
        while(true){
            System.out.println("Please input a search pattern:");
            System.out.print("> ");
            String pattern = Keyboard.readInput();
            pattern = "%" + pattern + "%";

            List<SimpledaoArticles> articles = articlesDAO.getArticlesByTitle(pattern);
            for (SimpledaoArticles a : articles) {
                System.out.println(a.getBody());
            }
        }

    }
}
