package ictgradschool.web.lab12.ex2;


public class SimpledaoArticles {

  private long artid;
  private String title;
  private String body;


  public long getArtid() {
    return artid;
  }

  public void setArtid(long artid) {
    this.artid = artid;
  }


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }


  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

}
