package ictgradschool.web.lab12.ex2;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class SimpledaoArticlesDAO {



    public List<SimpledaoArticles> getArticlesByTitle(String title){
        List<SimpledaoArticles> articles = new ArrayList<>();
        Properties dbProps = new Properties();

        try(FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");

            // Make a query
            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM simpledao_articles WHERE title like ?;")) {

                // Set the parameters of the query. Note that the indices are 1-based, not 0-based.
                stmt.setString(1, title);

                try (ResultSet r = stmt.executeQuery()) {
                    while(r.next()){
                        SimpledaoArticles a = new SimpledaoArticles();
                        a.setArtid(r.getInt(1));
                        a.setTitle(r.getString(2));
                        a.setBody(r.getString(3));
                        articles.add(a);
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return articles;
    }
}
